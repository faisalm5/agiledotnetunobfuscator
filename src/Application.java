import java.awt.EventQueue;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import generated.AgileDotNetType;

public class Application extends JTextField {

	private static final long serialVersionUID = 1L;
	private JTextArea obfuscatedTextPanel;
	private JTextArea unObfuscatedText;
	private JScrollPane obfuscatedTextScrollPane;
	private JScrollPane unObfuscatedTextScrollPane;
	private JFrame window;
	private JPanel panel;
	private JButton mappingFileBtn;
	private JFileChooser mappingFileChooser;
	private JTextField mappingFileLocation;
	private File mappingFile;

	public Application() throws HeadlessException {
		initialize();
	}
	
	public void initialize() {
		window = new JFrame();
		window.setResizable(false);
		window.setTitle("MUCUPPS Unobfuscator");
//		frame.setAlwaysOnTop(true);
		window.setBounds(100, 100, 879, 653);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel = new JPanel();
		panel.setLayout(null);
		window.getContentPane().add(panel);
		
		obfuscatedTextPanel = new JTextArea("Enter obfuscated text here...");
		obfuscatedTextPanel.setLineWrap(true);
		obfuscatedTextScrollPane = 
                new JScrollPane(obfuscatedTextPanel, 
                    ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, 
                    ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		obfuscatedTextScrollPane.setBounds(12, 69, 852, 200);
		panel.add(obfuscatedTextScrollPane);

		unObfuscatedText = new JTextArea("Unobfuscated text will appear here.");
		unObfuscatedText.setEditable(false);
		unObfuscatedText.setLineWrap(true);
		unObfuscatedTextScrollPane = 
                new JScrollPane(unObfuscatedText, 
                    ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, 
                    ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		unObfuscatedTextScrollPane.setBounds(12, 280, 852, 200);
		panel.add(unObfuscatedTextScrollPane);
		
		JButton unObfuscateButton = new JButton("Unobfuscate");
		unObfuscateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				unObfuscateText(e);
			}

		});
		unObfuscateButton.setLocation(366, 491);
		unObfuscateButton.setSize(131, 23);
//		unObfuscateButton.setBounds(214, 519, 131, 23);
//		unObfuscateButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		unObfuscateButton.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(unObfuscateButton);
		
		mappingFileBtn = new JButton("Select Mapping File");
		mappingFileBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectMappingFile(e);
			}
		});
		mappingFileBtn.setBounds(707, 11, 156, 23);
		panel.add(mappingFileBtn);
		
		mappingFileLocation = new JTextField();
		mappingFileLocation.setBounds(12, 11, 702, 23);
		mappingFileLocation.setEditable(false);
		panel.add(mappingFileLocation);
		mappingFileLocation.setColumns(10);

		
	}
	
	private void selectMappingFile(ActionEvent e){
		mappingFileChooser = new JFileChooser();
		mappingFileChooser.setCurrentDirectory(new File("C:\\Users\\fmohamme\\Documents\\AdapterLogs"));
		int result = mappingFileChooser.showOpenDialog(null);
        if (result==JFileChooser.APPROVE_OPTION) {
            mappingFile = mappingFileChooser.getSelectedFile();
    		mappingFileLocation.setEditable(true);
    		mappingFileLocation.setText(mappingFile.getAbsolutePath());		
    		mappingFileLocation.setEditable(false);

            loadMappingFile(mappingFile);
        }
	}
	
	private void loadMappingFile(File xmlFile) {
//		try {
//			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
//			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
//			Document doc = dBuilder.parse(xmlFile);
//			
			try {
			    //1. We need to create JAXContext instance
				JAXBContext jaxbContext = JAXBContext.newInstance(AgileDotNetType.class);
				
				  //2. Use JAXBContext instance to create the Unmarshaller.
			    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			    //3. Use the Unmarshaller to unmarshal the XML document to get an instance of JAXBElement.
			    JAXBElement<AgileDotNetType> unmarshalledObject = 
			        (JAXBElement<AgileDotNetType>)unmarshaller.unmarshal(xmlFile);

			    //4. Get the instance of the required JAXB Root Class from the JAXBElement.
			    AgileDotNetType obj = unmarshalledObject.getValue();
				
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
//		} catch (ParserConfigurationException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (SAXException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

	}

	private void unObfuscateText(ActionEvent e) {
		String obfuscatedText = obfuscatedTextPanel.getText();
		if (!obfuscatedText.equals("Enter obfuscated text here...")){
			if(mappingFile != null){
			unObfuscatedText.setEditable(true);
			unObfuscatedText.setText("Here");
			unObfuscatedText.setEditable(false);
			} else {
				JOptionPane.showMessageDialog(null, "Please select mapping xml file", "No Mapping File Found", JOptionPane.ERROR_MESSAGE);
			}
			
		}
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Application window = new Application();
					window.window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

//		
//		
//		
//		
//        // example text
//        String rep = "The quick brown fox jumps over the lazy dog.";
//        String all = rep;
//        for(int i = 0; i < 100; i++) 
//            all += "\n" + rep;
//
//        // create the line wrap example
////        JTextArea first = new JTextArea(all);
////        first.setLineWrap(true);
//
//        JTextArea firstText = new JTextArea(all);
//        firstText.setLineWrap(true);
//        JScrollPane first = 
//                new JScrollPane(firstText, 
//                    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
//                    JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
//
//        // create the scroll pane example
//        JScrollPane second = 
//            new JScrollPane(new JTextArea(all), 
//                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
//                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
//        
//        
//
//        // lay it out
//        JFrame f = new JFrame("Test");
//        f.getContentPane().setLayout(new GridLayout(1,2));
//        f.getContentPane().add(first);
//        f.getContentPane().add(second);
//        f.setSize(400, 300);
//        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        f.setVisible(true);
//    

	  }
}
